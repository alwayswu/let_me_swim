export interface Park{
    name: string;
    img: string;
    area: string;
    price: string;
    point: string;
    id: string;
    hot: string;
    intro: string;
    link: string;
    address: string;
    level: string;
    sold: string;
    objectId: string;
}