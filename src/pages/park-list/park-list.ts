import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { NavController, NavParams, InfiniteScroll, Refresher, Content, Img } from 'ionic-angular';
import { Park } from '../../providers/park-data/park';
import { ParkDataProvider } from '../../providers/park-data/park-data';
import { updateImgs } from 'ionic-angular/components/content/content';
import { ParkDetailPage } from '../park-detail/park-detail';


@Component({
  selector: 'page-park-list',
  templateUrl: 'park-list.html',
})
export class ParkListPage implements AfterViewInit {
  @ViewChild(Content) _content: Content;

  parks: Park[] = [];

  private num: number = 10;//每页显示记录数
  private curPage: number = 1;//当前页数
  private hasMore: boolean = true;//是否有更多数据

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public parkDataProvider: ParkDataProvider) {
  }

  ionViewDidLoad() {
    this.getParks(this.num, this.curPage);
  }


  getParks(num: number, page: number): void{
    this.parkDataProvider.getParks(num,page).subscribe( parks =>{
      // this.parks = parks;
      //保留原先数据
      if (parks.length > 0) {
        this.parks = this.parks.concat(parks);
      }else{
        this.hasMore = false;
      }
    });
  }


/**
 * 下拉刷新
 * @param refresh 
 */
  ionRefresh(refresh: Refresher){
    setTimeout(() => {
      this.curPage = 1;
      this.getParks(this.num, this.curPage);
      refresh.complete();
    }, 1000);
  }


  /**
   * 向下加载更多数据
   * @param infiniteScroll 
   */
  doInfinite(infiniteScroll: InfiniteScroll): void{
    if(!this.hasMore){
      infiniteScroll.complete();
      return;
    }

    setTimeout(() => {
      this.curPage +=1;
      this.getParks(this.num,this.curPage);
      infiniteScroll.complete();
    }, 500);
  }

  itemTapped(park: Park): void {
    this.navCtrl.push(ParkDetailPage, {park: park});
  }



  ngAfterViewInit(): void {
    //修补图片问题
    if(this._content){
      this._content.imgsUpdate = () =>{
        if(this._content._scroll.initialized && this._content._imgs.length &&
          this._content.isImgsUpdatable()){
            this._content._imgs.forEach((img: Img) => {img._rect = null});
            updateImgs(this._content._imgs, this._content._cTop*-1,
              this._content.contentHeight,this._content.directionY,1400,400);
        }
      };
    }
    
  }

}
